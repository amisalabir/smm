<?php
	require_once('./files/header.php');
?>
<link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
<link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
<section id="main-content">
	<section class="wrapper">
		<?php
			$stmt = $pdo->prepare('SELECT * FROM news ORDER BY NewsID DESC LIMIT 1');
			$stmt->execute();
			
			if($stmt->rowCount() > 0) {
				?>
				<div class="row">
					<div class="col-md-12">
						<div class="mini-stat clearfix">
							<span>
								<?php
									foreach($stmt->fetchAll() as $row) {
										echo '<a href="news.php"><strong style="font-size: 14px; color: #1ca59e;">'.$row['NewsTitle'].'</strong></a>';
										echo '<br>';
										echo $row['NewsContent'];
										echo '<hr>';
									}
								?>
							</span>
						</div>
					</div>
				</div>
				<?php
			}
		?>
		<!--mini statistics end-->
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						All Panel Services
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table">
							<section id="unseen">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped table-condensed">
									<thead>
										<tr>
											<th>Service ID</th>
											<th>Service Name</th>
											<th>Service Min. Quantity</th>
											<th>Service Price/Min. Quantity</th>
										</tr>
									</thead>
									<tbody>
										<?php
											$stmt = $pdo->prepare('SELECT * FROM products');
											$stmt->execute();
											
											$html = '';
											
											foreach($stmt->fetchAll() as $row) {
												$UserID = $user->GetData('UserID');
												$UserGroup = $user->GetData('UserLevel');
												$product_quantity = $row['ProductMinimumQuantity'];
												
												$stmt = $pdo->prepare('SELECT * FROM individualprices WHERE IPUserID = :IPUserID AND IPProductID = :IPProductID');
												$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $row['ProductID']));
												
												if($stmt->rowCount() == 1) {
													$IPPrice = $stmt->fetch(PDO::FETCH_ASSOC);
													$price = $product->DeclarePrice($IPPrice['IPPrice'], $product_quantity, $product_quantity);
												} else {
													if($UserGroup == 'reseller') {
														if(!empty($row['ProductResellerPrice']))
															$price = $product->DeclarePrice($row['ProductResellerPrice'], $product_quantity, $product_quantity);
														else
															$price = $product->DeclarePrice($row['ProductPrice'], $product_quantity, $product_quantity);
													} else {
														$price = $product->DeclarePrice($row['ProductPrice'], $product_quantity, $product_quantity);
													}
												}
												
												$price = round($price, 2);
												
												$html .= '<tr>';
												$html .= '<td>'.$row['ProductID'].'</td>';
												$html .= '<td>'.$row['ProductName'].'</td>';
												$html .= '<td>'.$row['ProductMinimumQuantity'].'</td>';
												$html .= '<td>$'.$price.'</td>';
												$html .= '</tr>';
											}
											
											echo $html;
										?>
									</tbody>
								</table>
							</section>
						</div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<?php
	require_once('./files/footer.php');
?>