<?php
	require_once('./files/header.php');
?>

<link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
<link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />

<section id="main-content">
	<section class="wrapper">
	<?php
		$stmt = $pdo->prepare('SELECT * FROM news ORDER BY NewsID DESC LIMIT 1');
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="mini-stat clearfix">
						<span>
							<?php
								foreach($stmt->fetchAll() as $row) {
									echo '<a href="news.php"><strong style="font-size: 14px; color: #1ca59e;">'.$row['NewsTitle'].'</strong></a>';
									echo '<br>';
									echo $row['NewsContent'];
									echo '<hr>';
								}
							?>
						</span>
					</div>
				</div>
			</div>
			<?php
		}
	?>
					
		<!--mini statistics end-->
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						Account Order History - Completed Orders
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						 </span>
					</header>
					<div class="panel-body">
						<div class="adv-table">
							<div class="space15"></div>
							<?php
								$UserID = $user->GetData('UserID');
								
								$stmt = $pdo->prepare('SELECT * FROM orders WHERE OrderUserID = :OrderUserID AND OrderStatus = "Completed" ORDER BY OrderDate DESC');
								$stmt->bindParam(':OrderUserID', $UserID);
								$stmt->execute();
								
								if($stmt->rowCount() > 0) {
							?>
								<section id="unseen">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped table-condensed" id="dynamic-orders">
										<thead>
											<tr>
												<th>Ordered Service</th>
												<th>Order Quantity</th>
												<th>Order Amount</th>
												<th>Order Link</th>
												<th>Order Date</th>
												<th>Order Status</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$html = '';
											
												foreach($stmt->fetchAll() as $row) {
													$html .= '<tr class="">';
													$html .= '<td>'.$product->GetData($row['OrderProductID'], 'ProductName').'</td>';
													$html .= '<td>'.$row['OrderQuantity'].'</td>';
													$html .= '<td>$'.round($row['OrderAmount'], 2).'</td>';
													$html .= '<td>'.$row['OrderLink'].'</td>';
													$html .= '<td>'.date('d M, Y h:I:s', $row['OrderDate']).'</td>';
													$html .= '<td class="center">'.$row['OrderStatus'].'.</td>';
													$html .= '</tr>';
												}
												
												echo $html;
											?>
										</tbody>
									</table>
								</section>
							<?php
								} else {
									$display->ReturnInfo('Your account does not have any completed orders at this time.');
								}
							?>
						</div>
						<button type="submit" id="page-refresh" class="btn btn-primary pull-right fa fa-refresh"></button>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<?php
	require_once('./files/footer.php');
?>