<?php
	require_once('./files/header.php');
?>
<section id="main-content">
	<section class="wrapper">
	<?php
		$stmt = $pdo->prepare('SELECT * FROM news ORDER BY NewsID DESC LIMIT 1');
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="mini-stat clearfix">
						<span>
							<?php
								foreach($stmt->fetchAll() as $row) {
									echo '<a href="news.php"><strong style="font-size: 14px; color: #1ca59e;">'.$row['NewsTitle'].'</strong></a>';
									echo '<br>';
									echo $row['NewsContent'];
									echo '<hr>';
								}
							?>
						</span>
					</div>
				</div>
			</div>
			<?php
		}
	?>
		<!-- Deposit Page -->

		<div class="row">
			<div class="col-sm-12">
				<section class="panel">
					<header class="panel-heading">
						Deposit Funds To User Account
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="col-md-12">
							<section class="panel">
								<div class="form-group">
									<div class="col-lg-8">
										<div class="input-group m-bot15">
											<span class="input-group-addon">$</span>
											<input type="number" id="amount" onchange="updateInput(this.value)" class="form-control" value="1" placeholder="1.00" autocomplete="off" required>
										</div>
									</div>
								</div>
								<?php
									if(!empty($PaypalEmail)) {
								?>
									<div class="form-group" style="display: inline-block;">
										<form method="POST" action="https://www.paypal.com/cgi-bin/webscr">
											<input type="hidden" name="cmd" value="_xclick">
											<input type="hidden" name="business" value="<?php echo $PaypalEmail; ?>">
											<input type="hidden" name="no_shipping" value="1">
											<input type="hidden" name="quantity" value="1">
											<input type="hidden" name="page_style" value="primary">
											<input type="hidden" name="no_note" value="Test this">
											<input type="hidden" name="cancel_return" value="<?php echo $settings->url(); ?>/paypal.php">
											<input type="hidden" name="return" value="<?php echo $settings->url(); ?>/paypal.php">
											<input type="hidden" name="notify_url" value="<?php echo $settings->url(); ?>/paypal.php">
											<input type="hidden" name="lc" value="US">
											<input type="hidden" name="currency_code" value="USD">
											<input type="hidden" id="new_amount" name="amount" value="1">
											<input type="hidden" name="item_name" value="Funds Deposit.">
											<input type="submit" id="paypal" class="btn btn-info" value="Paypal Deposit">
										</form>
									</div>
								<?php
									}
									
									if(!empty($SkrillEmail) && !empty($SkrillSecret)) {
								?>
									<div class="form-group" style="display: inline-block;">
										<form method="POST" action="https://www.moneybookers.com/app/payment.pl">
											<input type="hidden" name="pay_to_email" value="<?php echo $SkrillEmail; ?>"/>
											<input type="hidden" name="status_url" value="<?php echo $settings->url(); ?>/skrill.php"/> 
											<input type="hidden" name="language" value="EN"/>
											<input type="hidden" id="new_amount" name="amount" value="1"/>
											<input type="hidden" name="currency" value="USD"/>
											<input type="hidden" name="detail1_description" value="Deposit funds to account."/>
											<input type="hidden" name="detail1_text" value="Skrill Deposit"/>
											<input type="submit" id="skrill" class="btn btn-danger" value="Skrill Deposit">
										</form>
									</div>
								<?php
									}
									
									if(empty($PaypalEmail) && empty($SkrillEmail) && empty($SkrillSecret)) {
										?>
											<div class="form-group" style="display: inline-block;">
												<a href="support.php" class="btn btn-primary">Use our support system.</a>
											</div>
										<?php
									}
								?>
							</section>
						</div>
						<div id="result"></div>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<?php
	require_once('./files/footer.php');
?>
<script>
function updateInput(ish){
    document.getElementById("new_amount").value = ish;
}
</script>