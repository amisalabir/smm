<?php
	require_once('./files/header.php');
?>

<link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
<link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />

<section id="main-content">
	<section class="wrapper">
	<?php
		$stmt = $pdo->prepare('SELECT * FROM news ORDER BY NewsID DESC LIMIT 1');
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="mini-stat clearfix">
						<span>
							<?php
								foreach($stmt->fetchAll() as $row) {
									echo '<a href="news.php"><strong style="font-size: 14px; color: #1ca59e;">'.$row['NewsTitle'].'</strong></a>';
									echo '<br>';
									echo $row['NewsContent'];
									echo '<hr>';
								}
							?>
						</span>
					</div>
				</div>
			</div>
			<?php
		}
	?>
					
		<!--mini statistics end-->
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						Account Order History - In Process Orders
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						 </span>
					</header>
					<div class="panel-body">
						<div class="adv-table">
							<div class="space15"></div>
							<?php
								$UserID = $user->GetData('UserID');
								
								$stmt = $pdo->prepare('SELECT * FROM orders WHERE OrderUserID = :OrderUserID AND OrderStatus != "Completed" AND OrderStatus != "Canceled" ORDER BY OrderDate DESC');
								$stmt->bindParam(':OrderUserID', $UserID);
								$stmt->execute();
								
								if($stmt->rowCount() > 0) {
							?>
								<section id="unseen">
									<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped table-condensed" id="dynamic-orders">
										<thead>
											<tr>
												<th>Ordered Service</th>
												<th>Order Quantity</th>
												<th>Order Amount</th>
												<th>Order Link</th>
												<th>Order Date</th>
												<th>Order Status</th>
												<th>Order Remains</th>
												<th>Order Start Count</th>
											</tr>
										</thead>
										<tbody>
											<?php
												$html = '';
											
												foreach($stmt->fetchAll() as $row) {
													if(empty($row['OrderAPIID'])) {
														$status = $row['OrderStatus'];
														$start_count = 0;
														$remains = 0;
													} else if($row['OrderStatus'] == 'In Process') {
														$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
														$stmt->execute(array(':ProductID' => $row['OrderProductID']));
														$service_api = $stmt->fetch();
														
														$parts = parse_url($service_api['ProductAPI']);
														parse_str($parts['query'], $query);
														$api_key = $query['key'];
														
														$current_url = explode("?", $service_api['ProductAPI']);
														$url = $current_url[0].'?key='.$api_key.'&action=status&order='.$row['OrderAPIID'];
														
														$curl = curl_init();
														curl_setopt_array($curl, array(
															CURLOPT_RETURNTRANSFER => 1,
															CURLOPT_URL => $url,
															CURLOPT_USERAGENT => 'Enigma SMM API Caller'
														));
														
														$resp = curl_exec($curl);
														curl_close($curl);
														
														$response = json_decode($resp);
														if(isset($response->status))
															$status = $response->status;
														else
															$status = 'Completed';
														if(isset($response->remains))
															$remains = $response->remains;
														else
															$remains = 0;
														if(isset($response->start_count) && empty($row['OrderStartCount']))
															$start_count = $response->start_count;
														else if(!isset($response->start_count) && !empty($row['OrderStartCount']))
															$start_count = $row['OrderStartCount'];
														else if(isset($response->start_count) && !empty($row['OrderStartCount']))
															$start_count = $row['OrderStartCount'];
														else
															$start_count = 0;
														
														$UserFunds = $user->GetData('UserFunds');
														
														if($status == 'Completed') {
															$stmt = $pdo->prepare('UPDATE orders SET OrderStatus = "Completed" WHERE OrderID = :OrderID');
															$stmt->execute(array(':OrderID' => $row['OrderID']));
														} else if($status == 'Canceled' || $status == 'Refunded') {
															if($status == 'Canceled') {
																$stmt = $pdo->prepare('UPDATE orders SET OrderStatus = "Removed" WHERE OrderID = :OrderID');
																$stmt->execute(array(':OrderID' => $row['OrderID']));
															} else if($status == 'Refunded') {
																$stmt = $pdo->prepare('UPDATE orders SET OrderStatus = "Refunded" WHERE OrderID = :OrderID');
																$stmt->execute(array(':OrderID' => $row['OrderID']));
															}
															
															$stmt = $pdo->prepare('UPDATE users SET UserFunds = :UserFunds WHERE UserID = :UserID');
															$stmt->execute(array(':UserFunds' => $row['OrderAmount'] + $UserFunds, ':UserID' => $UserID));
														}
													} else {
														$status = $row['OrderStatus'];
														$start_count = 0;
														$remains = 0;
													}
													
													$html .= '<tr class="">';
													$html .= '<td>'.$product->GetData($row['OrderProductID'], 'ProductName').'</td>';
													$html .= '<td>'.$row['OrderQuantity'].'</td>';
													$html .= '<td>$'.round($row['OrderAmount'], 2).'</td>';
													$html .= '<td>'.$row['OrderLink'].'</td>';
													$html .= '<td>'.date('d M, Y h:I:s', $row['OrderDate']).'</td>';
													$html .= '<td class="center">'.$status.'.</td>';
													$html .= '<td class="center">'.$remains.'</td>';
													$html .= '<td class="center">'.$start_count.'</td>';
													$html .= '</tr>';
												}
												
												echo $html;
											?>
										</tbody>
									</table>
								</section>
							<?php
								} else {
									$display->ReturnInfo('Your account does not have any in process orders at this time.');
								}
							?>
						</div>
						<button type="submit" id="page-refresh" class="btn btn-primary pull-right fa fa-refresh"></button>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<?php
	require_once('./files/footer.php');
?>