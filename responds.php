<?php
require_once('./files/functions.php');

/* RESET ACCOUNT PASSWORD */

if(isset($_POST['action']) && $_POST['action'] == 'reset') {
	if(isset($_POST['username']) && isset($_POST['email'])
		&& is_string($_POST['username']) && is_string($_POST['email'])
		&& !empty($_POST['username']) && !empty($_POST['email'])) {
			
		if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
			$username = stripslashes(strip_tags($_POST['username']));
			$email = $_POST['email'];
			
			$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName OR UserEmail = :UserEmail');
			$stmt->execute(array(':UserName' => $username, ':UserEmail' => $email));
			
			if($stmt->rowCount() > 0) {
				$new_password = substr(md5(rand(1,100000)), 0, 8);
				
				$stmt = $pdo->prepare('UPDATE users SET UserPassword = :UserPassword WHERE UserName = :UserName');
				$stmt->execute(array(':UserPassword' => md5($new_password), ':UserName' => $username));
				
				$subject = 'Password recovery';
				$txt = 'Your account password has been reset.';
				$txt .= 'Your new account password is: '.$new_password.'';
				$headers = "From: ".$RecoveryEmail."" . "\r\n" .
				"CC: ".$RecoveryEmail."";

				mail($email,$subject,$txt,$headers);
			} else {
				echo('User with these credentials does not exists.');
			}
		} else {
			echo('The entered E-mail is invalid.');
		}
	} else {
		echo('Fill all fields correctly.');
	}
}

/* SAVE MERCHANT */

if(isset($_POST['action']) && $_POST['action'] == 'save-merchant') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['website-name']) && isset($_POST['recovery-email']) &&
		is_string($_POST['website-name']) && is_string($_POST['recovery-email']) &&
		!empty($_POST['website-name']) && !empty($_POST['recovery-email'])) {
			if(!filter_var($_POST['recovery-email'], FILTER_VALIDATE_EMAIL) === false) {
				$WebsiteName = $_POST['website-name'];
				$RecoveryEmail = $_POST['recovery-email'];
				
					$PaypalEmail = $_POST['paypal-email'];
				
					$SkrillEmail = $_POST['skrill-email'];
					$SkrillSecret = $_POST['skrill-secret'];
				
				$stmt = $pdo->prepare('SELECT * FROM merchant');
				$stmt->execute();
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				
				if(empty($row['MerchantWebsiteName'])) {
					$stmt = $pdo->prepare('INSERT INTO merchant (MerchantWebsiteName, MerchantRecoveryEmail, MerchantPaypalEmail, MerchantSkrillEmail, MerchantSkrillSecret)
					VALUES (:MerchantWebsiteName, :MerchantRecoveryEmail, :MerchantPaypalEmail, :MerchantSkrillEmail, :MerchantSkrillSecret)');
					
					$stmt->execute(array(':MerchantWebsiteName' => $WebsiteName, ':MerchantRecoveryEmail' => $RecoveryEmail, ':MerchantPaypalEmail' => $PaypalEmail,
					':MerchantSkrillEmail' => $SkrillEmail, ':MerchantSkrillSecret' => $SkrillSecret));
				} else {
					$CurrentName = $row['MerchantWebsiteName'];
					
					$stmt = $pdo->prepare('UPDATE merchant SET MerchantWebsiteName = :MerchantWebsiteName, MerchantRecoveryEmail = :MerchantRecoveryEmail,
					MerchantPaypalEmail = :MerchantPaypalEmail, MerchantSkrillEmail = :MerchantSkrillEmail, MerchantSkrillSecret = :MerchantSkrillSecret WHERE MerchantWebsiteName = :MerchantWebsiteNameConfirm');
					
					$stmt->execute(array(':MerchantWebsiteName' => $WebsiteName, ':MerchantRecoveryEmail' => $RecoveryEmail, ':MerchantPaypalEmail' => $PaypalEmail,
					':MerchantSkrillEmail' => $SkrillEmail, ':MerchantSkrillSecret' => $SkrillSecret, ':MerchantWebsiteNameConfirm' => $CurrentName));
				}
			} else {
				echo('The provided recovery E-mail address is invalid.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* CREATE USER FROM ADMINISTRATION PANEL */

if(isset($_POST['action']) && $_POST['action'] == 'create-user') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['user-first-name']) && isset($_POST['user-last-name']) && isset($_POST['user-email']) && isset($_POST['user-name']) && isset($_POST['user-password']) && isset($_POST['user-level']) && isset($_POST['user-funds']) &&
		is_string($_POST['user-first-name']) && is_string($_POST['user-last-name']) && is_string($_POST['user-email']) && is_string($_POST['user-name']) && is_string($_POST['user-password']) && is_string($_POST['user-level']) && is_numeric($_POST['user-funds']) &&
		!empty($_POST['user-first-name']) && !empty($_POST['user-last-name']) && !empty($_POST['user-email']) && !empty($_POST['user-name']) && !empty($_POST['user-password']) && !empty($_POST['user-level'])) {
			if(strlen($_POST['user-password']) < 32 && strlen($_POST['user-password']) > 3) {
				if(strlen($_POST['user-name']) < 16 && strlen($_POST['user-name']) > 3) {
					if(!filter_var($_POST['user-email'], FILTER_VALIDATE_EMAIL) === false) {
						$first_name = stripslashes(strip_tags($_POST['user-first-name']));
						$last_name = stripslashes(strip_tags($_POST['user-last-name']));
						$email = $_POST['user-email'];
						$user_name = stripslashes(strip_tags($_POST['user-name']));
						$password = md5($_POST['user-password']);
						$level = stripslashes(strip_tags($_POST['user-level']));
						$funds = stripslashes(strip_tags($_POST['user-funds']));
						
						$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName OR UserEmail = :UserEmail');
						$stmt->execute(array(':UserName' => $user_name, ':UserEmail' => $email));
						
						if($stmt->rowCount() == 0) {
							$stmt = $pdo->prepare('INSERT INTO users (UserName, UserEmail, UserPassword, UserLevel, UserFirstName, UserLastName, UserRegistrationDate, UserRegistrationAddress, UserFunds)
							VALUES (:UserName, :UserEmail, :UserPassword, :UserLevel, :UserFirstName, :UserLastName, :UserRegistrationDate, :UserRegistrationAddress, :UserFunds)');
							
							$stmt->execute(array(':UserName' => $user_name, ':UserEmail' => $email, ':UserPassword' => $password, ':UserLevel' => $level, ':UserFirstName' => $first_name, ':UserLastName' => $last_name, ':UserRegistrationDate' => time(), ':UserRegistrationAddress' => '127.0.0.1', ':UserFunds' => $funds));	
						} else {
							echo('User with these credentials already exists.');
							return false;
						}
					} else {
						echo('The provided e-mail address is invalid.');
					}
				} else {
					echo('User name length have to be 4-16 characters.');
				}
			} else {
				echo('Password length have to be 4-32 characters.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* ADD NEW */

if(isset($_POST['action']) && $_POST['action'] == 'add-new') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['new-title']) && isset($_POST['new-content']) &&
		is_string($_POST['new-title']) && is_string($_POST['new-content']) &&
		!empty($_POST['new-title']) && !empty($_POST['new-content'])) {
			$new_title = stripslashes(strip_tags($_POST['new-title']));
			$new_content = stripslashes(strip_tags($_POST['new-content']));
			$new_user = $user->GetData('UserID');
			$new_date = time();
			
			$stmt = $pdo->prepare('INSERT INTO news (NewsTitle, NewsContent, NewsDate, NewsUserID) VALUES (:NewsTitle, :NewsContent, :NewsDate, :NewsUserID)');
			$stmt->execute(array(':NewsTitle' => $new_title, ':NewsContent' => $new_content, ':NewsDate' => $new_date, ':NewsUserID' => $new_user));
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* EDIT NEW */

if(isset($_POST['action']) && $_POST['action'] == 'edit-new') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['new-id']) && isset($_POST['new-title']) && isset($_POST['new-content']) &&
		is_string($_POST['new-title']) && is_string($_POST['new-content']) &&
		!empty($_POST['new-title']) && !empty($_POST['new-content'])) {
			$new_title = stripslashes(strip_tags($_POST['new-title']));
			$new_content = stripslashes(strip_tags($_POST['new-content']));
			$new_id = $_POST['new-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM news WHERE NewsID = :NewsID');
			$stmt->bindParam(':NewsID', $new_id);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$stmt = $pdo->prepare('UPDATE news SET NewsTitle = :NewsTitle, NewsContent = :NewsContent WHERE NewsID = :NewsID');
				
				$stmt->execute(array(':NewsTitle' => $new_title, ':NewsContent' => $new_content, ':NewsID' => $new_id));
			} else {
				echo('New does not exists.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* DELETE NEW */

if(isset($_POST['action']) && $_POST['action'] == 'delete-new') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['new-id']) && !empty($_POST['new-id']) && ctype_digit($_POST['new-id'])) {
			$NewsID = $_POST['new-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM news WHERE NewsID = :NewsID');
			$stmt->bindParam(':NewsID', $NewsID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$stmt = $pdo->prepare('DELETE FROM news WHERE NewsID = :NewsID');
				$stmt->bindParam(':NewsID', $NewsID);
				$stmt->execute();
			} else {
				echo 'New does not exists.';
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* DELETE LOGS */

if(isset($_POST['action']) && $_POST['action'] == 'delete-logs') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		$stmt = $pdo->prepare('DELETE FROM logs');
		$stmt->execute();
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* EDIT USER */

if(isset($_POST['action']) && $_POST['action'] == 'edit-user') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['user-id']) && isset($_POST['user-first-name']) && isset($_POST['user-last-name']) && isset($_POST['user-email']) && isset($_POST['user-name']) && isset($_POST['user-level']) && isset($_POST['user-funds']) &&
		is_string($_POST['user-first-name']) && is_string($_POST['user-last-name']) && is_string($_POST['user-email']) && is_string($_POST['user-name']) && is_string($_POST['user-level']) && preg_match('/^[0-9.]+$/', $_POST['user-funds']) &&
		!empty($_POST['user-id']) && !empty($_POST['user-first-name']) && !empty($_POST['user-last-name']) && !empty($_POST['user-email']) && !empty($_POST['user-name']) && !empty($_POST['user-level'])) {
			if(strlen($_POST['user-name']) < 16 && strlen($_POST['user-name']) > 3) {
				if(!filter_var($_POST['user-email'], FILTER_VALIDATE_EMAIL) === false) {
					$first_name = stripslashes(strip_tags($_POST['user-first-name']));
					$last_name = stripslashes(strip_tags($_POST['user-last-name']));
					$email = $_POST['user-email'];
					$user_name = stripslashes(strip_tags($_POST['user-name']));
					$level = stripslashes(strip_tags($_POST['user-level']));
					$funds = stripslashes(strip_tags($_POST['user-funds']));
					$user_id = $_POST['user-id'];
					
					$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName OR UserEmail = :UserEmail');
					$stmt->execute(array(':UserName' => $user_name, ':UserEmail' => $email));
					
					
					$query = $pdo->prepare('SELECT * FROM users WHERE UserID = :UserID');
					$query->bindParam(':UserID', $user_id);
					$query->execute();
					
					if($query->rowCount() == 0) {
						echo 'User account does not exists.';
						return false;
					}
					if($stmt->rowCount() <= 1) {
						$stmt = $pdo->prepare('UPDATE users SET UserFirstName = :UserFirstName, UserLastName = :UserLastName, UserEmail = :UserEmail, UserName = :UserName, UserLevel = :UserLevel, UserFunds = :UserFunds WHERE UserID = :UserID');
						
						$stmt->execute(array(':UserFirstName' => $first_name, ':UserLastName' => $last_name, ':UserEmail' => $email,
						':UserName' => $user_name, ':UserLevel' => $level, ':UserFunds' => $funds, ':UserID' => $user_id));
					} else {
						echo('User with these credentials already exists.');
						return false;
					}
				} else {
					echo('The provided e-mail address is invalid.');
				}
			} else {
				echo('User name length have to be 4-16 characters.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* CREATE CATEGORY */

if(isset($_POST['action']) && $_POST['action'] == 'create-category') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['category-name']) && isset($_POST['category-description']) &&
		is_string($_POST['category-name']) && is_string($_POST['category-description']) &&
		!empty($_POST['category-name']) && !empty($_POST['category-description'])) {
			$category_name = stripslashes(strip_tags($_POST['category-name']));
			$category_description = stripslashes(strip_tags($_POST['category-description']));
			$time = time();
			
			$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryName = :CategoryName');
			$stmt->bindParam(':CategoryName', $category_name);
			$stmt->execute();
			
			if($stmt->rowCount() == 0) {
				$stmt = $pdo->prepare('INSERT INTO categories (CategoryName, CategoryDescription, CategoryCreatedDate) VALUES (:CategoryName, :CategoryDescription, :CategoryCreatedDate)');
				$stmt->execute(array(':CategoryName' => $category_name, ':CategoryDescription' => $category_description, ':CategoryCreatedDate' => $time));
			} else {
				echo('Category already exists.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* OPEN TICKET */

if(isset($_POST['action']) && $_POST['action'] == 'open-ticket') {
	if(isset($_POST['ticket-title']) && isset($_POST['ticket-message']) &&
	is_string($_POST['ticket-title']) && is_string($_POST['ticket-message']) &&
	!empty($_POST['ticket-title']) && !empty($_POST['ticket-message'])) {
		$ticket_title = stripslashes(strip_tags($_POST['ticket-title']));
		$ticket_message = stripslashes(strip_tags($_POST['ticket-message']));
		$time = time();
		$user_id = $user->GetData('UserID');
		
		$stmt = $pdo->prepare('INSERT INTO support (SupportUserID, SupportTitle, SupportMessage, SupportDate) VALUES (:SupportUserID, :SupportTitle, :SupportMessage, :SupportDate)');
		$stmt->execute(array(':SupportUserID' => $user_id, ':SupportTitle' => $ticket_title, ':SupportMessage' => $ticket_message, ':SupportDate' => $time));
	} else {
		echo('Fill all fields correctly.');
	}
}

/* TICKET REPLY */

if(isset($_POST['action']) && $_POST['action'] == 'reply-ticket') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['ticket-id']) && isset($_POST['ticket-reply']) &&
		ctype_digit($_POST['ticket-id']) && is_string($_POST['ticket-reply']) &&
		!empty($_POST['ticket-id']) && !empty($_POST['ticket-reply'])) {
			$ticket_id = $_POST['ticket-id'];
			$ticket_reply = stripslashes(strip_tags($_POST['ticket-reply']));
			
			$stmt = $pdo->prepare('UPDATE support SET SupportReply = :SupportReply WHERE SupportID = :SupportID');
			$stmt->execute(array(':SupportReply' => $ticket_reply, ':SupportID' => $ticket_id));
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* TICKET DELETE */

if(isset($_POST['action']) && $_POST['action'] == 'delete-ticket') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['ticket-id']) && ctype_digit($_POST['ticket-id']) && !empty($_POST['ticket-id'])) {
			$ticket_id = $_POST['ticket-id'];
			
			$stmt = $pdo->prepare('DELETE FROM support WHERE SupportID = :SupportID');
			$stmt->bindParam(':SupportID', $ticket_id);
			$stmt->execute();
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* CREATE SERVICE */

if(isset($_POST['action']) && $_POST['action'] == 'create-service') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['service-name']) && isset($_POST['service-description']) && isset($_POST['service-quantity']) && isset($_POST['service-price']) && isset($_POST['service-category']) && isset($_POST['service-api']) && isset($_POST['service-reseller-price']) && isset($_POST['service-max-quantity']) &&
		is_string($_POST['service-name']) && is_string($_POST['service-description']) && is_string($_POST['service-quantity']) && is_string($_POST['service-price']) && is_string($_POST['service-category']) && is_string($_POST['service-max-quantity']) &&
		!empty($_POST['service-name']) && !empty($_POST['service-description']) && !empty($_POST['service-quantity']) && ctype_digit($_POST['service-quantity']) && !empty($_POST['service-price']) && !empty($_POST['service-category']) && !empty($_POST['service-max-quantity'])) {
			$service_name = stripslashes(strip_tags($_POST['service-name']));
			$service_description = stripslashes(strip_tags($_POST['service-description']));
			$service_quantity = stripslashes(strip_tags($_POST['service-quantity']));
			$service_max_quantity = stripslashes(strip_tags($_POST['service-max-quantity']));
			$service_price = stripslashes(strip_tags($_POST['service-price']));
			$service_category = stripslashes(strip_tags($_POST['service-category']));
			$service_api = htmlspecialchars($_POST['service-api']);
			$service_reseller = $_POST['service-reseller-price'];
			$time = time();
			
			$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductName = :ProductName');
			$stmt->bindParam(':ProductName', $service_name);
			$stmt->execute();
			
			if($stmt->rowCount() == 0) {
				$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryID = :CategoryID');
				$stmt->bindParam(':CategoryID', $service_category);
				$stmt->execute();
				
				if($stmt->rowCount() > 0 ) {
					if($service_max_quantity > $service_quantity) {
						$stmt = $pdo->prepare('INSERT INTO products (ProductCategoryID, ProductName, ProductDescription, ProductMinimumQuantity, ProductMaxQuantity, ProductPrice, ProductAPI, ProductCreatedDate)
						VALUES (:ProductCategoryID, :ProductName, :ProductDescription, :ProductMinimumQuantity, :ProductMaxQuantity, :ProductPrice, :ProductAPI, :ProductCreatedDate)');
						
						$stmt->execute(array(':ProductCategoryID' => $service_category, ':ProductName' => $service_name, ':ProductDescription' => $service_description,
						':ProductMinimumQuantity' => $service_quantity, ':ProductMaxQuantity' => $service_max_quantity, ':ProductPrice' => $service_price,
						':ProductAPI' => $service_api, ':ProductCreatedDate' => $time));
					} else {
						echo 'Service max quantity have to be bigger than the minimum quantity.';
					}
				} else {
					echo 'Category does not exists.';
				}
			} else {
				echo('Service already exists.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* EDIT SERVICE */

if(isset($_POST['action']) && $_POST['action'] == 'edit-service') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['service-id']) && isset($_POST['service-name']) && isset($_POST['service-description']) && isset($_POST['service-quantity']) && isset($_POST['service-price']) && isset($_POST['service-category']) && isset($_POST['service-api']) && isset($_POST['service-reseller-price']) && isset($_POST['service-max-quantity']) &&
		is_string($_POST['service-name']) && is_string($_POST['service-description']) && is_string($_POST['service-quantity']) && is_string($_POST['service-price']) && is_string($_POST['service-category']) && is_string($_POST['service-max-quantity']) &&
		!empty($_POST['service-name']) && !empty($_POST['service-description']) && !empty($_POST['service-quantity']) && ctype_digit($_POST['service-quantity']) && !empty($_POST['service-price']) && !empty($_POST['service-category']) && !empty($_POST['service-max-quantity'])) {
			$service_name = stripslashes(strip_tags($_POST['service-name']));
			$service_description = stripslashes(strip_tags($_POST['service-description']));
			$service_quantity = stripslashes(strip_tags($_POST['service-quantity']));
			$service_max_quantity = stripslashes(strip_tags($_POST['service-max-quantity']));
			$service_price = stripslashes(strip_tags($_POST['service-price']));
			$service_category = stripslashes(strip_tags($_POST['service-category']));
			$service_api = $_POST['service-api'];
			$service_reseller = $_POST['service-reseller-price'];
			$time = time();
			
			$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
			$stmt->bindParam(':ProductID', $_POST['service-id']);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$ServiceRow = $stmt->fetch(PDO::FETCH_ASSOC);
			
				$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductName = :ProductName');
				$stmt->bindParam(':ProductName', $service_name);
				$stmt->execute();
				
				if(strtolower($ServiceRow['ProductName']) == strtolower($service_name) || $stmt->rowCount() == 0) {
					$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryID = :CategoryID');
					$stmt->bindParam(':CategoryID', $service_category);
					$stmt->execute();
					
					if($stmt->rowCount() == 1) {
						$stmt = $pdo->prepare('UPDATE products SET ProductCategoryID = :ProductCategoryID, ProductName = :ProductName, ProductDescription = :ProductDescription, ProductMinimumQuantity = :ProductMinimumQuantity, ProductPrice = :ProductPrice, ProductAPI = :ProductAPI, ProductResellerPrice = :ProductResellerPrice, ProductMaxQuantity = :ProductMaxQuantity WHERE ProductID = :ProductID');
						$stmt->execute(array(':ProductCategoryID' => $service_category, ':ProductName' => $service_name, ':ProductDescription' => $service_description,
						':ProductMinimumQuantity' => $service_quantity, ':ProductPrice' => $service_price, ':ProductID' => $_POST['service-id'],
						':ProductAPI' => $service_api, ':ProductResellerPrice' => $service_reseller, ':ProductMaxQuantity' => $service_max_quantity));
					} else {
						echo 'Category does not exists.';
					}
				} else {
					echo 'Service with this name already exists.';
				}
			} else {
				echo('Service does not exists.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* DELETE SERVICE */

if(isset($_POST['action']) && $_POST['action'] == 'delete-service') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['service-id']) && !empty($_POST['service-id']) && ctype_digit($_POST['service-id'])) {
			$ServiceID = $_POST['service-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
			$stmt->bindParam(':ProductID', $ServiceID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$stmt = $pdo->prepare('DELETE FROM products WHERE ProductID = :ProductID');
				$stmt->bindParam(':ProductID', $ServiceID);
				$stmt->execute();
			} else {
				echo 'Service does not exists.';
				return false;
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'get-max-quantity') {
	if(isset($_POST['service']) && is_numeric($_POST['service'])) {
		$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
		$stmt->execute(array(':ProductID' => $_POST['service']));
		
		if($stmt->rowCount() == 1) {
			$row = $stmt->fetch();
			
			echo $row['ProductMaxQuantity'];
		}
	}
}

/* EDIT CATEGORY */

if(isset($_POST['action']) && $_POST['action'] == 'edit-category') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['category-id']) && isset($_POST['category-name']) && isset($_POST['category-description']) &&
		is_string($_POST['category-name']) && is_string($_POST['category-description']) &&
		!empty($_POST['category-name']) && !empty($_POST['category-description'])) {
			$category_id = stripslashes(strip_tags($_POST['category-id']));
			$category_name = stripslashes(strip_tags($_POST['category-name']));
			$category_description = stripslashes(strip_tags($_POST['category-description']));
			$time = time();
			
			$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryID = :CategoryID');
			$stmt->bindParam(':CategoryID', $_POST['category-id']);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$CategoryRow = $stmt->fetch(PDO::FETCH_ASSOC);
				
				$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryName = :CategoryName');
				$stmt->bindParam(':CategoryName', $category_name);
				$stmt->execute();
				
				if(strtolower($CategoryRow['CategoryName']) == strtolower($category_name) || $stmt->rowCount() == 0) {
					$stmt = $pdo->prepare('UPDATE categories SET CategoryName = :CategoryName, CategoryDescription = :CategoryDescription WHERE CategoryID = :CategoryID');
					$stmt->execute(array(':CategoryID' => $category_id, ':CategoryName' => $category_name, ':CategoryDescription' => $category_description));
				} else {
					echo('Category name already exists.');
				}
			} else {
				echo('Category already exists.');
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* DELETE CATEGORY */

if(isset($_POST['action']) && $_POST['action'] == 'delete-category') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['category-id']) && !empty($_POST['category-id']) && ctype_digit($_POST['category-id'])) {
			$CategoryID = $_POST['category-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryID = :CategoryID');
			$stmt->bindParam(':CategoryID', $CategoryID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$stmt = $pdo->prepare('DELETE FROM categories WHERE CategoryID = :CategoryID');
				$stmt->bindParam(':CategoryID', $CategoryID);
				$stmt->execute();
				
				$stmt = $pdo->prepare('DELETE FROM products WHERE ProductCategoryID = :ProductCategoryID');
				$stmt->bindParam(':ProductCategoryID', $CategoryID);
				$stmt->execute();
			} else {
				echo 'Category does not exists.';
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* UPDATE ORDER STATUS */


if(isset($_POST['action']) && $_POST['action'] == 'update-order-status') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['order-status']) && !empty($_POST['order-status']) && is_string($_POST['order-status']) &&
			isset($_POST['order-id']) && !empty($_POST['order-id']) && ctype_digit($_POST['order-id'])) {	
			$OrderID = $_POST['order-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM orders WHERE OrderID = :OrderID');
			$stmt->bindParam(':OrderID', $OrderID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$row = $stmt->fetch();
				
				$OrderStatus = $_POST['order-status'];
				if($OrderStatus == 'Delete Order') {
					$stmt = $pdo->prepare('DELETE FROM orders WHERE OrderID = :OrderID');
					$stmt->bindParam(':OrderID', $OrderID);
					$stmt->execute();
				}
				if($OrderStatus == 'Refunded') {
					$UserID = $user->GetData('UserID');
					$UserFunds = $user->GetData('UserFunds');
					$stmt = $pdo->prepare('UPDATE users SET UserFunds = :UserFunds WHERE UserID = :UserID');
					$stmt->execute(array(':UserFunds' => $row['OrderAmount'] + $UserFunds, ':UserID' => $UserID));
				}
				$stmt = $pdo->prepare('UPDATE orders SET OrderStatus = :OrderStatus WHERE OrderID = :OrderID');
				$stmt->execute(array(':OrderStatus' => $OrderStatus, ':OrderID' => $OrderID));
			} else {
				echo 'Order does not exists.';
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* ADD INDIVUDUAL PRICE */

if(isset($_POST['action']) && $_POST['action'] == 'add-individual-price') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['ip-username']) && isset($_POST['ip-service']) && isset($_POST['ip-price']) &&
		is_string($_POST['ip-username']) && ctype_digit($_POST['ip-service']) && is_numeric($_POST['ip-price']) &&
		!empty($_POST['ip-username']) && !empty($_POST['ip-service'])) {
			$username = $_POST['ip-username'];
			$service = $_POST['ip-service'];
			$price = $_POST['ip-price'];
			
			$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName');
			$stmt->bindParam(':UserName', $username);
			$stmt->execute();
			
			if($stmt->rowCount() > 0) {
				$UserRow = $stmt->fetch(PDO::FETCH_ASSOC);
				$UserID = $UserRow['UserID'];
				
				$stmt = $pdo->prepare('SELECT * FROM individualprices WHERE IPUserID = :IPUserID AND IPProductID = :IPProductID');
				$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $service));
				
				if($stmt->rowCount() == 0) {
					$stmt = $pdo->prepare('INSERT INTO individualprices (IPUserID, IPProductID, IPPrice) VALUES (:IPUserID, :IPProductID, :IPPrice)');
					$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $service, ':IPPrice' => $price));
				} else {
					echo 'Individual price for this user with this service already exists.';
				}
			} else {
				echo 'User name does not exists.';
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* EDIT INDIVUDUAL PRICE */

	if(isset($_POST['action']) && $_POST['action'] == 'edit-individual-price') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['ip-username']) && isset($_POST['ip-service']) && isset($_POST['ip-price']) && isset($_POST['ip-id']) &&
		is_string($_POST['ip-username']) && ctype_digit($_POST['ip-service']) && is_numeric($_POST['ip-price']) &&
		!empty($_POST['ip-username']) && !empty($_POST['ip-service'])) {
			$id = $_POST['ip-id'];
			$username = $_POST['ip-username'];
			$service = $_POST['ip-service'];
			$price = $_POST['ip-price'];
			
			$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName');
			$stmt->bindParam(':UserName', $username);
			$stmt->execute();
			
			if($stmt->rowCount() > 0) {
				$UserRow = $stmt->fetch(PDO::FETCH_ASSOC);
				$UserID = $UserRow['UserID'];
				
				$stmt = $pdo->prepare('SELECT * FROM individualprices WHERE IPUserID = :IPUserID AND IPProductID = :IPProductID');
				$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $service));
				
				$cs = $pdo->prepare('SELECT * FROM individualprices  WHERE IPID = :IPID');
				$cs->bindParam(':IPID', $id);
				$cs->execute();
				$csr = $cs->fetch(PDO::FETCH_ASSOC);
				
				if($stmt->rowCount() == 0 || $service == $csr['IPProductID']) {
					$stmt = $pdo->prepare('UPDATE individualprices SET IPUserID = :IPUserID, IPProductID = :IPProductID, IPPrice = :IPPrice WHERE IPID = :IPID');
					$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $service, ':IPPrice' => $price, ':IPID' => $id));
				} else {
					echo 'Individual price for this user with this service already exists.';
				}
			} else {
				echo 'User name does not exists.';
			}
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* INDIVIDUAL PRICE DELETE */

if(isset($_POST['action']) && $_POST['action'] == 'delete-ip') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['ip-id']) && !empty($_POST['ip-id']) && ctype_digit($_POST['ip-id'])) {
			$IPID = $_POST['ip-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM individualprices WHERE IPID = :IPID');
			$stmt->bindParam(':IPID', $IPID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$stmt = $pdo->prepare('DELETE FROM individualprices WHERE IPID = :IPID');
				$stmt->bindParam(':IPID', $IPID);
				$stmt->execute();
			} else {
				echo 'Individual prices does not exists.';
				return false;
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* BAN & UNBAN USER */

if(isset($_POST['action']) && $_POST['action'] == 'ban-user') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['user-id']) && !empty($_POST['user-id']) && ctype_digit($_POST['user-id'])) {
			$UserID = $_POST['user-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM users WHERE UserID = :UserID');
			$stmt->bindParam(':UserID', $UserID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				$UserLevel = $row['UserLevel'];
				
				if($UserLevel == 'banned') {
					echo 'User account is already terminated.';
					return false;
				} else {
					$stmt = $pdo->prepare('UPDATE users SET UserLevel = :UserLevel WHERE UserID = :UserID');
					$stmt->execute(array(':UserLevel' => 'banned', ':UserID' => $UserID));
				}
			} else {
				echo 'User account does not exists.';
				return false;
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

if(isset($_POST['action']) && $_POST['action'] == 'unban-user') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['user-id']) && !empty($_POST['user-id']) && ctype_digit($_POST['user-id'])) {
			$UserID = $_POST['user-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM users WHERE UserID = :UserID');
			$stmt->bindParam(':UserID', $UserID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$row = $stmt->fetch(PDO::FETCH_ASSOC);
				$UserLevel = $row['UserLevel'];
				
				if($UserLevel != 'banned') {
					echo 'User account is not terminated.';
					return false;
				} else {
					$stmt = $pdo->prepare('UPDATE users SET UserLevel = :UserLevel WHERE UserID = :UserID');
					$stmt->execute(array(':UserLevel' => 'default', ':UserID' => $UserID));
				}
			} else {
				echo 'User account does not exists.';
				return false;
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* DELETE USER */
		
if(isset($_POST['action']) && $_POST['action'] == 'delete-user') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['user-id']) && !empty($_POST['user-id']) && ctype_digit($_POST['user-id'])) {
			$UserID = $_POST['user-id'];
			
			$stmt = $pdo->prepare('SELECT * FROM users WHERE UserID = :UserID');
			$stmt->bindParam(':UserID', $UserID);
			$stmt->execute();
			
			if($stmt->rowCount() == 1) {
				$stmt = $pdo->prepare('DELETE FROM users WHERE UserID = :UserID');
				$stmt->bindParam(':UserID', $UserID);
				$stmt->execute();
			} else {
				echo 'User account does not exists.';
				return false;
			}
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* UPDATE PROFILE INFORMATION */

if(isset($_POST['action']) && $_POST['action'] == 'profile-update') {
	if(isset($_POST['first-name']) && isset($_POST['last-name']) && isset($_POST['email']) && isset($_POST['password'])
		&& is_string($_POST['first-name']) && is_string($_POST['last-name']) && is_string($_POST['email']) && is_string($_POST['password'])
		&& !empty($_POST['first-name']) && !empty($_POST['last-name']) && !empty($_POST['email']) && !empty($_POST['password'])) {
		
		if(md5($_POST['password']) == $user->GetData('UserPassword')) {
			if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) === false) {
				$first_name = stripslashes(strip_tags($_POST['first-name']));
				$last_name = stripslashes(strip_tags($_POST['last-name']));
				$email = $_POST['email'];
				$UserID = $user->GetData('UserID');
				
				$stmt = $pdo->prepare('UPDATE users SET UserFirstName = :UserFirstName, UserLastName = :UserLastName, UserEmail = :UserEmail WHERE UserID = :UserID');
				$stmt->execute(array(':UserFirstName' =>$first_name, ':UserLastName' => $last_name, ':UserEmail' => $email, ':UserID' => $UserID));
			} else {
				echo('The provided E-mail is invalid.');
			}
		} else {
			echo('The entered password does not equals to your account password.');
		}
	} else {
		echo('Fill all fields correctly.');
	}
}

/* UPDATE ACCOUNT PASSWORD */

if(isset($_POST['action']) && $_POST['action'] == 'password-update') {
	if(isset($_POST['current-password']) && isset($_POST['new-password'])
		&& is_string($_POST['current-password']) && is_string($_POST['new-password'])
		&& !empty($_POST['current-password']) && !empty($_POST['new-password'])) {
		
		if(md5($_POST['current-password']) == $user->GetData('UserPassword')) {
			if(strlen($_POST['new-password']) > 3 && strlen($_POST['new-password']) < 32) {
				$UserID = $user->GetData('UserID');
				
				$stmt = $pdo->prepare('UPDATE users SET UserPassword = :UserPassword WHERE UserID = :UserID');
				$stmt->execute(array(':UserPassword' => md5($_POST['new-password']), ':UserID' => $UserID));
			} else {
				echo('Password length have to be 4-32 characters.');
			}
		} else {
			echo('The entered password does not match to your account password.');
		}
	} else {
		echo('Fill all fields correctly.');
	}
}

/* GET AVAILABLE SERVICES */

if(isset($_POST['action']) && $_POST['action'] == 'get-products') {
	$category = stripslashes(strip_tags($_POST['option']));
	
	$stmt = $pdo->prepare('SELECT * FROM categories WHERE CategoryID = :CategoryID');
	$stmt->bindParam(':CategoryID', $category);
	$stmt->execute();
	
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$CategoryID = $row['CategoryID'];
	
	$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductCategoryID = :ProductCategoryID');
	$stmt->bindParam(':ProductCategoryID', $CategoryID);
	$stmt->execute();
	
	$html = '';
	
	foreach($stmt->fetchAll() as $rows) {
		$html .= '<option value="'.$rows['ProductID'].'">'.$rows['ProductName'].'</option>';		
	}
	
	echo $html;
}

/* CREATE SERVICE ORDER */

if(isset($_POST['action']) && $_POST['action'] == 'create-order') {
	if(isset($_POST['service']) && isset($_POST['quantity']) && isset($_POST['link']) &&
	!empty($_POST['service']) && !empty($_POST['quantity']) && !empty($_POST['link']) &&
	ctype_digit($_POST['service']) && ctype_digit($_POST['quantity']) && is_string($_POST['link'])) {
		$service = strip_tags(stripslashes($_POST['service']));
		$quantity = strip_tags(stripslashes($_POST['quantity']));
		$link = stripslashes(strip_tags($_POST['link']));
		$time = time();
		$UserID = $user->GetData('UserID');
		$UserName = $user->GetData('UserName');
		$UserGroup = $user->GetData('UserLevel');
		
		$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
		$stmt->bindParam(':ProductID', $service);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$max_quantity = $row['ProductMaxQuantity'];
			$product_quantity = $row['ProductMinimumQuantity'];
			$account_balance = $user->GetData('UserFunds');
			
			if($quantity >= $product_quantity) {
				if($quantity <= $max_quantity) {
					$query = $pdo->prepare('SELECT * FROM orders WHERE OrderLink = :OrderLink AND OrderProductID = :OrderProductID');
					$query->execute(array(':OrderLink' => $link, ':OrderProductID' => $service));
					
					if($query->rowCount() > 0) {
						if($query->rowCount() == 1) {
							$query_row = $query->fetch();
							$qu_am = $query_row['OrderQuantity'];
						} else {
							$qu_am = 0;
							
							foreach($query->fetchAll() as $qu_row) {
								$qu_am += $qu_row['OrderQuantity'];
							}
						}
						$total = $qu_am + $quantity;
						$total_more = $max_quantity - $qu_am;
						if($total_more < 0) {
							$total_more = 0;
						}
						
						if($total > $max_quantity) {
							echo 'You can purchase '.$total_more.' more.';
							exit();
						}
					}
					
					$stmt = $pdo->prepare('SELECT * FROM individualprices WHERE IPUserID = :IPUserID  AND IPProductID = :IPProductID');
					$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $service));
						
					if($stmt->rowCount() == 1) {
						$IPPrice = $stmt->fetch(PDO::FETCH_ASSOC);
						$newprice = $product->DeclarePrice($IPPrice['IPPrice'], $row['ProductMinimumQuantity'], $quantity);
					} else {
						if($UserGroup == 'reseller') {
							if(!empty($row['ProductResellerPrice']))
								$newprice = $product->DeclarePrice($row['ProductResellerPrice'], $row['ProductMinimumQuantity'], $quantity);
							else
								$newprice = $product->DeclarePrice($row['ProductPrice'], $row['ProductMinimumQuantity'], $quantity);
						} else {
							$newprice = $product->DeclarePrice($row['ProductPrice'], $row['ProductMinimumQuantity'], $quantity);
						}
					}
					$price = round($newprice, 2);
					if($account_balance >= $price) {
						$api = $row['ProductAPI'];
						
						if(!empty($api)) {
							$api_link = str_replace('[LINK]', $link, $api);
							$api_final = str_replace('[QUANTITY]', $quantity, $api_link);
							
							$curl = curl_init();
							curl_setopt_array($curl, array(
								CURLOPT_RETURNTRANSFER => 1,
								CURLOPT_URL => $api_final,
								CURLOPT_USERAGENT => 'Enigma SMM API Caller'
							));
							
							$resp = curl_exec($curl);
							curl_close($curl);
							$resp = json_decode($resp);
							if(isset($resp->order)) {
								$order_id = $resp->order;
								
								$stmt = $pdo->prepare('INSERT INTO orders (OrderUserID, OrderProductID, OrderDate,
								OrderLink, OrderQuantity, OrderAmount, OrderStatus, OrderAPIID) VALUES (:OrderUserID, :OrderProductID, :OrderDate, :OrderLink, :OrderQuantity, :OrderAmount, :OrderStatus, :OrderAPIID)');
								
								$stmt->execute(array(':OrderUserID' => $UserID, ':OrderProductID' => $service, ':OrderDate' => $time, ':OrderLink' => $link,
								':OrderQuantity' => $quantity, ':OrderAmount' => $price, ':OrderStatus' => 'In Process', 'OrderAPIID' => $order_id));
							} else {
								$stmt = $pdo->prepare('INSERT INTO orders (OrderUserID, OrderProductID, OrderDate, OrderLink, OrderQuantity, OrderAmount) VALUES (:OrderUserID, :OrderProductID, :OrderDate, :OrderLink, :OrderQuantity, :OrderAmount)');
								$stmt->execute(array(':OrderUserID' => $UserID, ':OrderProductID' => $service, ':OrderDate' => $time, ':OrderLink' => $link, ':OrderQuantity' => $quantity, ':OrderAmount' => $price));
							}
						} else {
							$stmt = $pdo->prepare('INSERT INTO orders (OrderUserID, OrderProductID, OrderDate, OrderLink, OrderQuantity, OrderAmount) VALUES (:OrderUserID, :OrderProductID, :OrderDate, :OrderLink, :OrderQuantity, :OrderAmount)');
							$stmt->execute(array(':OrderUserID' => $UserID, ':OrderProductID' => $service, ':OrderDate' => $time, ':OrderLink' => $link, ':OrderQuantity' => $quantity, ':OrderAmount' => $price));
						}
						// Take balance from user's account
						
						$UserFunds = $account_balance - $price;
						
						$stmt = $pdo->prepare('UPDATE users SET UserFunds = :UserFunds WHERE UserID = :UserID');
						$stmt->execute(array(':UserFunds' => $UserFunds, ':UserID' => $UserID));
						
						$ProductName = $product->GetData($service, 'ProductName');
						
						$subject = "New Service Order";
						$txt = "User with ID: #".$UserID." and username: ".$UserName.".\r\n";
						$txt .= "Service ID: ".$service.", service name: ".$ProductName.".\r\n";
						$txt .= "Quantity: ".$quantity.".\r\n";
						$txt .= "Link: ".$link."\r\n";
						$headers = "From: purchase@smm-panel.net" . "\r\n" .
						"CC: purchase@smm-panel.net";

						@mail($NotificationEmail,$subject,$txt,$headers);
					} else {
						echo 'Not enough funds in the account.You can deposit funds to your account from <a href="./deposit.php">here</a>.';
					}
				} else {
					echo 'Maximum quantity for this service: '.$max_quantity;
				}
			} else {
				echo 'Minimum product quantity for purchase is '.$product_quantity.'.';
			}
		} else {
			echo 'Invalid Product ID.';
		}
	} else {
		echo 'Fill all fields correctly.';
	}
}

/* ADD NAVIGATION LINK */

if(isset($_POST['action']) && $_POST['action'] == 'add-navigation') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['nav-text']) && isset($_POST['nav-url']) && isset($_POST['nav-icon']) &&
		is_string($_POST['nav-text']) && is_string($_POST['nav-url']) && is_string($_POST['nav-icon']) &&
		!empty($_POST['nav-text']) && !empty($_POST['nav-url']) && !empty($_POST['nav-icon'])) {
			$NavText = stripslashes(strip_tags($_POST['nav-text']));
			$NavURL = stripslashes(strip_tags($_POST['nav-url']));
			$NavIcon = stripslashes(strip_tags($_POST['nav-icon']));
			
			$stmt = $pdo->prepare('INSERT INTO navigation (NavigationText, NavigationURL, NavigationIcon)
			VALUES (:NavigationText, :NavigationURL, :NavigationIcon)');
			$stmt->execute(array(':NavigationText' => $NavText, ':NavigationURL' => $NavURL, ':NavigationIcon' => $NavIcon));
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* EDIT NAVIGATION LINK */

if(isset($_POST['action']) && $_POST['action'] == 'edit-navigation') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['nav-text']) && isset($_POST['nav-url']) && isset($_POST['nav-icon']) && isset($_POST['nav-id']) &&
		is_string($_POST['nav-text']) && is_string($_POST['nav-url']) && is_string($_POST['nav-icon']) && is_numeric($_POST['nav-id']) &&
		!empty($_POST['nav-text']) && !empty($_POST['nav-url']) && !empty($_POST['nav-icon'])) {
			$NavID = stripslashes(strip_tags($_POST['nav-id']));
			$NavText = stripslashes(strip_tags($_POST['nav-text']));
			$NavURL = stripslashes(strip_tags($_POST['nav-url']));
			$NavIcon = stripslashes(strip_tags($_POST['nav-icon']));
			
			$stmt = $pdo->prepare('UPDATE navigation SET NavigationText = :NavigationText, NavigationURL = :NavigationURL, NavigationIcon = :NavigationIcon WHERE NavigationID = :NavigationID');
			$stmt->execute(array(':NavigationText' => $NavText, ':NavigationURL' => $NavURL, ':NavigationIcon' => $NavIcon, ':NavigationID' => $NavID));
		} else {
			echo('Fill all fields correctly.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* DELETE NAVIGATION LINK */

if(isset($_POST['action']) && $_POST['action'] == 'delete-navigation') {
	$UserLevel = $user->GetData('UserLevel');
	
	if($UserLevel == 'admin') {
		if(isset($_POST['nav-id']) && is_numeric($_POST['nav-id'])) {
			$NavID = stripslashes(strip_tags($_POST['nav-id']));
			
			$stmt = $pdo->prepare('SELECT * FROM navigation WHERE NavigationID = :NavigationID');
			$stmt->execute(array(':NavigationID' => $NavID));
			
			if($stmt->rowCount() > 0) {
				$stmt = $pdo->prepare('DELETE FROM navigation WHERE NavigationID = :NavigationID');
				$stmt->execute(array(':NavigationID' => $NavID));
			} else {
				echo('Navigation link does not exists.');
			}
		} else {
			echo('Error.');
		}
	} else {
		echo('You don\'t have permissions to browse this page.');
	}
}

/* GET ORDER AMOUNT */

if(isset($_POST['action']) && $_POST['action'] == 'get-amount') {
	if(isset($_POST['service']) && isset($_POST['quantity']) &&
	!empty($_POST['service']) && !empty($_POST['quantity'])&&
	ctype_digit($_POST['service']) && ctype_digit($_POST['quantity'])) {
		$service = strip_tags(stripslashes($_POST['service']));
		$quantity = strip_tags(stripslashes($_POST['quantity']));
		$UserGroup = $user->GetData('UserLevel');
		$UserID = $user->GetData('UserID');
		
		$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
		$stmt->bindParam(':ProductID', $service);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$product_quantity = $row['ProductMinimumQuantity'];
			
			if($quantity >= $product_quantity) {
				$stmt = $pdo->prepare('SELECT * FROM individualprices WHERE IPUserID = :IPUserID AND IPProductID = :IPProductID');
				$stmt->execute(array(':IPUserID' => $UserID, ':IPProductID' => $service));
					
				if($stmt->rowCount() == 1) {
					$IPPrice = $stmt->fetch(PDO::FETCH_ASSOC);
					$price = $product->DeclarePrice($IPPrice['IPPrice'], $product_quantity, $quantity);
				} else {
					if($UserGroup == 'reseller') {
						if(!empty($row['ProductResellerPrice']))
							$price = $product->DeclarePrice($row['ProductResellerPrice'], $product_quantity, $quantity);
						else
							$price = $product->DeclarePrice($row['ProductPrice'], $product_quantity, $quantity);
					} else {
						$price = $product->DeclarePrice($row['ProductPrice'], $product_quantity, $quantity);
					}
				}
				echo round($price, 2);
			} else {
				echo 'Invalid quantity.';
			}
		} else {
			echo 'Invalid Product ID.';
		}
	} else {
		echo 'Fill all fields correctly.';
	}
}

/* GET PRODUCT DETAILS (SUCH AS QUANTITY, PRICE) */

if(isset($_POST['action']) && $_POST['action'] == 'product-details') {
	if(isset($_POST['details']) && isset($_POST['product-id']) && !empty($_POST['details']) && !empty($_POST['product-id']) && is_string($_POST['details']) && ctype_digit($_POST['product-id'])) {
		$Details = strip_tags(stripslashes($_POST['details']));
		$ProductID = strip_tags(stripslashes($_POST['product-id']));
		
		$stmt = $pdo->prepare('SELECT * FROM  products WHERE ProductID = :ProductID');
		$stmt->bindParam(':ProductID', $ProductID);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			echo $row[$Details];
		} else {
			echo 'Error.';
		}
	}
}

/* UPDATE ACCOUNT AVATAR */

if(is_array($_FILES) && isset($_FILES['avatar'])) {
	if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
		if(($_FILES['avatar']['type'] == 'image/gif') || ($_FILES['avatar']['type'] == 'image/jpeg') || ($_FILES['avatar']['type'] == 'image/png')) {
			$image_info = getimagesize($_FILES["avatar"]["tmp_name"]);
			$image_width = $image_info[0];
			$image_height = $image_info[1];
			if($image_width > 512 && $image_height > 512) {
				echo 'Maximum image size: width: 512px & height: 512px.';
			} else {
				$image = addslashes(file_get_contents($_FILES['avatar']['tmp_name']));
				$UserID = $user->GetData('UserID');
				
				$stmt = $pdo->prepare('UPDATE users SET UserImage = :UserImage WHERE UserID = :UserID');
				$stmt->execute(array(':UserImage' => $image, ':UserID' => $UserID));
			}
		} else {
			echo 'Image format not supported, or image is corrupt.';
		}
	} else {
		echo 'An error occurred.';
	}
}