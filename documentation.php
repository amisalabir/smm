<?php
	require_once('./files/header.php');
	
	$url = $settings->url();
?>
<link href="js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
<link href="js/advanced-datatable/css/demo_table.css" rel="stylesheet" />
<section id="main-content">
	<section class="wrapper">
		<?php
		$stmt = $pdo->prepare('SELECT * FROM news ORDER BY NewsID DESC LIMIT 1');
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="mini-stat clearfix">
						<span>
							<?php
								foreach($stmt->fetchAll() as $row) {
									echo '<a href="news.php"><strong style="font-size: 14px; color: #1ca59e;">'.$row['NewsTitle'].'</strong></a>';
									echo '<br>';
									echo $row['NewsContent'];
									echo '<hr>';
								}
							?>
						</span>
					</div>
				</div>
			</div>
			<?php
		}
	?>
		<!--mini statistics end-->
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading">
						API Documentation
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table">
							<section id="unseen">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped table-condensed">
									<thead>
										<tr>
											<th>HTTP Method</th>
											<th>API URL</th>
											<th>Response format</th>

										</tr>
									</thead>
									<tbody>
										<tr>
											<td>GET</td>
											<td><?php echo $url; ?>/api.php</td>
											<td>JSON</td>
										</tr>
									</tbody>
								</table>
							</section>
							<h2>Example Response: </h2>
							<code>
								{"order":"162"}
							</code>
						</div>
					</div>
					
					<header class="panel-heading">
						Create Order
						<span class="tools pull-right">
							<a href="javascript:;" class="fa fa-chevron-down"></a>
							<a href="javascript:;" class="fa fa-times"></a>
						</span>
					</header>
					<div class="panel-body">
						<div class="adv-table">
							<section id="unseen">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped table-condensed">
									<thead>
										<tr>
											<th>key</th>
											<th>service</th>
											<th>quantity</th>
											<th>link</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Your API Key.</br>You can find it on your profile page.</td>
											<td>Service ID.<br>You can find it on services page.</td>
											<td>Needed quantity.</td>
											<td>Link to page.</td>
										</tr>
									</tbody>
								</table>
								<h2>Example Response: </h2>
								<code>
									{"charge":"0.015","status":"In Process", "link":"http://youtube.com/some_user", "quantity":"1000"}
								</code>
							</section>
						</div>
					</div>
				</section>
			</div>
			
		</div>
	</section>
</section>
<?php
	require_once('./files/footer.php');
?>