<?php

require_once('./files/functions.php');

$messages = array();

if(isset($_GET['key']) && !empty($_GET['key']) && ctype_alnum($_GET['key']) && isset($_GET['action']) && $_GET['action'] == 'order' &&
	isset($_GET['quantity']) && !empty($_GET['quantity']) && ctype_digit($_GET['quantity']) && $_GET['quantity'] > 0 &&
	isset($_GET['service']) && !empty($_GET['service']) && ctype_digit($_GET['service']) &&
	isset($_GET['link']) && !empty($_GET['link']) && is_string($_GET['link'])) {
	
	$APIKey = stripslashes(strip_tags($_GET['key']));
	$stmt = $pdo->prepare('SELECT * FROM users WHERE UserAPI = :UserAPI');
	$stmt->bindParam(':UserAPI', $APIKey);
	$stmt->execute();
	
	if($stmt->rowCount() > 0) {
		$Quantity = stripslashes(strip_tags($_GET['quantity']));
		$Service = stripslashes(strip_tags($_GET['service']));
		$Link = html_entity_decode($_GET['link']);
		$Time = time();
		
		$UserRow = $stmt->fetch();
		$UserID = $UserRow['UserID'];
		$UserFunds = $UserRow['UserFunds'];
		
		$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
		$stmt->bindParam(':ProductID', $Service);
		$stmt->execute();
		
		if($stmt->rowCount() > 0) {
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			$product_quantity = $row['ProductMinimumQuantity'];
			$ProductName = $row['ProductName'];
			$account_balance = $UserRow['UserFunds'];
			
			if($Quantity >= $product_quantity) {
				$newprice = $product->DeclarePrice($row['ProductPrice'], $row['ProductMinimumQuantity'], $Quantity);
				$price = round($newprice, 2);
				if($account_balance >= $price) {
					$api = $row['ProductAPI'];
					
					if(!empty($api)) {
						$api_link = str_replace('[LINK]', $link, $api);
						$api_final = str_replace('[QUANTITY]', $quantity, $api_link);
						
						$curl = curl_init();
						curl_setopt_array($curl, array(
							CURLOPT_RETURNTRANSFER => 1,
							CURLOPT_URL => $api_final,
							CURLOPT_USERAGENT => 'Enigma SMM API Caller'
						));
						
						$resp = curl_exec($curl);
						curl_close($curl);
						
						$stmt = $pdo->prepare('INSERT INTO orders (OrderUserID, OrderProductID, OrderDate, OrderLink, OrderQuantity, OrderAmount, OrderStatus) VALUES (:OrderUserID, :OrderProductID, :OrderDate, :OrderLink, :OrderQuantity, :OrderAmount, :OrderStatus)');
						$stmt->execute(array(':OrderUserID' => $UserID, ':OrderProductID' => $Service, ':OrderDate' => $Time, ':OrderLink' => $Link, ':OrderQuantity' => $Quantity, ':OrderAmount' => $price, ':OrderStatus' => 'Completed'));
					} else {
						$stmt = $pdo->prepare('INSERT INTO orders (OrderUserID, OrderProductID, OrderDate, OrderLink, OrderQuantity, OrderAmount) VALUES (:OrderUserID, :OrderProductID, :OrderDate, :OrderLink, :OrderQuantity, :OrderAmount)');
						$stmt->execute(array(':OrderUserID' => $UserID, ':OrderProductID' => $Service, ':OrderDate' => $Time, ':OrderLink' => $Link, ':OrderQuantity' => $Quantity, ':OrderAmount' => $price));
					}
					
					$order_id = $pdo->lastInsertId();
					
					$UserFunds = $account_balance - $price;
					
					$stmt = $pdo->prepare('UPDATE users SET UserFunds = :UserFunds WHERE UserID = :UserID');
					$stmt->execute(array(':UserFunds' => $UserFunds, ':UserID' => $UserID));
					echo('{"order":"'.$order_id.'"}');
				} else {
					echo('{"error":"Not enough account balance"}');
				}
			} else {
				echo('{"error":"Minimum quantity is '.$product_quantity.'"}');
			}
		} else {
			echo('{"error":"Invalid service"}');
		}
	} else {
		echo('{"error":"Unknwon API key"}');
	}
} else if(isset($_GET['action']) && $_GET['action'] == 'status' && isset($_GET['order']) && ctype_digit($_GET['order']) &&
	isset($_GET['key']) && !empty($_GET['order'])) {
		$stmt = $pdo->prepare('SELECT * FROM users WHERE UserAPI = :UserAPI');
		$stmt->execute(array(':UserAPI' => $_GET['key']));
		
		if($stmt->rowCount() == 1) {
			$user_row = $stmt->fetch();
			$UserID = $user_row['UserID'];
			
			$stmt = $pdo->prepare('SELECT * FROM orders WHERE OrderID = :OrderID AND OrderUserID = :OrderUserID');
			$stmt->execute(array(':OrderID' => $_GET['order'], ':OrderUserID' => $UserID));
			
			if($stmt->rowCount() == 1) {
				$order_row = $stmt->fetch();
				if(empty($row['OrderAPIID'])) {
					$status = $row['OrderStatus'];
					$start_count = $row['OrderStartCount'];
					$remains = 0;
				} else if($row['OrderStatus'] == 'In Process') {
					$stmt = $pdo->prepare('SELECT * FROM products WHERE ProductID = :ProductID');
					$stmt->execute(array(':ProductID' => $row['OrderProductID']));
					$service_api = $stmt->fetch();
					
					$parts = parse_url($service_api['ProductAPI']);
					parse_str($parts['query'], $query);
					$api_key = $query['key'];
					
					$current_url = explode("?", $service_api['ProductAPI']);
					$url = $current_url[0].'?key='.$api_key.'&action=status&order='.$row['OrderAPIID'];
					
					$curl = curl_init();
					curl_setopt_array($curl, array(
						CURLOPT_RETURNTRANSFER => 1,
						CURLOPT_URL => $url,
						CURLOPT_USERAGENT => 'Enigma SMM API Caller'
					));
					
					$resp = curl_exec($curl);
					curl_close($curl);
					
					$response = json_decode($resp);
					if(isset($response->status))
						$status = $response->status;
					else
						$status = $row['OrderStatus'];
					
					if(isset($response->remains))
						$remains = $response->remains;
					else
						$remains = 0;
					if(empty($row['OrderStartCount']) && $row['OrderStartCount'] == 0) {
						if(isset($response->start_count))
							$start_count = $response->start_count;
						else
							$start_count = 0;
					} else {
							$start_count = $row['OrderStartCount'];
					}
					
					if(!empty($row['OrderStartCount']) && isset($response->start_count)) {
						$start_count = $row['OrderStartCount'];
					}
					
					if($status == 'Completed') {
						$stmt = $pdo->prepare('UPDATE orders SET OrderStatus = "Completed" WHERE OrderID = :OrderID');
						$stmt->execute(array(':OrderID' => $row['OrderID']));
					} else if($status == 'Canceled') {
						$stmt = $pdo->prepare('UPDATE orders SET OrderStatus = "Removed" WHERE OrderID = :OrderID');
						$stmt->execute(array(':OrderID' => $row['OrderID']));
					}
				} else {
					$status = $row['OrderStatus'];
					$start_count = $row['OrderStartCount'];
					$remains = 0;
				}
				echo('{"charge":"'.$order_row['OrderAmount'].'",
				"status":"'.$status.'",
				"link":"'.$order_row['OrderLink'].'",
				"quantity":"'.$order_row['OrderQuantity'].'",
				"start_count":"'.$start_count.'",
				"remains":"'.$remains.'"');
			} else {
				echo('{"error":"Unknwon order id"}');
			}
		} else {
			echo('{"error":"Unknwon API key"}');
		}
} else {
	echo('{"error":"Incorrect request"}');
}