<?php
	require_once('./files/functions.php');
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="Social Panel">
		<link rel="shortcut icon" href="images/favicon.png">
		<title><?php echo($WebsiteName); ?> | Sign In</title>
		<link href="bs3/css/bootstrap.min.css" rel="stylesheet">
		<link href="css/bootstrap-reset.css" rel="stylesheet">
		<link href="font-awesome/css/font-awesome.css" rel="stylesheet" />
		<link href="css/style.css" rel="stylesheet">
		<link href="css/style-responsive.css" rel="stylesheet" />
		
		<!--[if lt IE 9]>
			<script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
	</head>
	<body class="login-body">
		<div class="container">
			<form class="form-signin" method="POST">
				<h2 class="form-signin-heading">sign in now</h2>
				<div class="login-wrap">
					<div class="user-login-info">
						<input type="text" name="username" class="form-control" placeholder="User Name" autofocus required>
						<input type="password" name="password" class="form-control" placeholder="Password" required>
					</div>
					<label class="checkbox">
						<input type="checkbox" value="remember-me"> Remember me
						<span class="pull-right">
							<a data-toggle="modal" href="#myModal"> Forgot Password?</a>
						</span>
					</label>
					<input type="submit" name="login" value="Sign In" class="btn btn-lg btn-login btn-block">
					<div class="registration">
						Don't have an account yet?
						<a class="" href="registration.php">Create an account</a>
					</div>
				</div>
			</form>
			<?php
				if(isset($_POST['login'])) {
					if(isset($_POST['username']) && isset($_POST['password']) &&
					is_string($_POST['username']) && is_string($_POST['password']) &&
					!empty($_POST['username']) && !empty($_POST['password'])) {
						$username = stripslashes(strip_tags($_POST['username']));
						$password = md5($_POST['password']);
						
						$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName');
						$stmt->bindParam(':UserName', $username);
						$stmt->execute();
						
						if($stmt->rowCount() > 0) {
							$stmt = $pdo->prepare('SELECT * FROM users WHERE UserName = :UserName AND UserPassword = :UserPassword');
							$stmt->execute(array(':UserName' => $username, ':UserPassword' => $password));
							
							if($stmt->rowCount() > 0) {
								$row = $stmt->fetch();
								$UserLevel = $row['UserLevel'];
								
								if($UserLevel == 'banned') {
									$display->ReturnError('Your account has been suspended.');
									return false;
								}
								$UserID = $row['UserID'];
								$time = time();
								$IPAddress = $_SERVER['REMOTE_ADDR'];
								
								$_SESSION['auth'] = $UserID;
								
								$stmt = $pdo->prepare('INSERT INTO logs (LogUserID, LogDate, LogIPAddress) VALUES (:LogUserID, :LogDate, :LogIPAddress)');
								$stmt->execute(array(':LogUserID' => $UserID, ':LogDate' => $time, ':LogIPAddress' => $IPAddress));
								
								$display->ReturnSuccess('You was successfully logged in.');
								$settings->forceRedirect('index.php', 2);
							} else {
								$display->ReturnError('Invalid user credentials.');
							}
						} else {
							$display->ReturnError('User with these credentials does not exists.');
						}
					}
				}
			?>
		</div>
		
		<script src="js/jquery.js"></script>
		<script src="bs3/js/bootstrap.min.js"></script>
		<script src="js/sm-requests.js"></script>
		
		<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
			<form method="POST">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Forgot Password ?</h4>
						</div>
						<div class="modal-body">
							<p>Enter your user name below.</p>
							<input type="text" id="username" name="username" placeholder="User Name" class="form-control placeholder-no-fix" autocomplete="off" required>
						</div>
						<div class="modal-body">
							<p>Enter your e-mail address below.</p>
							<input type="email" id="email" name="email" placeholder="Email" class="form-control placeholder-no-fix" autocomplete="off" required>
						</div>
						<div class="modal-footer">
							<button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
							<button id="reset" class="btn btn-primary" type="button">Reset</button>
							<hr>
							<div id="result"></div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</body>
</html>