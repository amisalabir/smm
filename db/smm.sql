/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `CategoryID` int(244) NOT NULL AUTO_INCREMENT,
  `CategoryName` blob NOT NULL,
  `CategoryDescription` blob NOT NULL,
  `CategoryCreatedDate` int(244) NOT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `deposits` */

DROP TABLE IF EXISTS `deposits`;

CREATE TABLE `deposits` (
  `DepositID` int(244) NOT NULL AUTO_INCREMENT,
  `DepositUserID` int(244) NOT NULL,
  `DepositDate` int(244) NOT NULL,
  `DepositAmount` varchar(244) CHARACTER SET latin1 NOT NULL,
  `DepositVerification` blob NOT NULL,
  `DepositGateway` enum('Paypal','Skrill') NOT NULL,
  PRIMARY KEY (`DepositID`),
  KEY `deposituserid` (`DepositUserID`),
  CONSTRAINT `deposituserid` FOREIGN KEY (`DepositUserID`) REFERENCES `users` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `individualprices` */

DROP TABLE IF EXISTS `individualprices`;

CREATE TABLE `individualprices` (
  `IPID` int(244) NOT NULL AUTO_INCREMENT,
  `IPUserID` int(244) NOT NULL,
  `IPProductID` int(244) NOT NULL,
  `IPPrice` blob NOT NULL,
  PRIMARY KEY (`IPID`),
  KEY `ipuserid` (`IPUserID`),
  KEY `ipproductid` (`IPProductID`),
  CONSTRAINT `ipproductid` FOREIGN KEY (`IPProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ipuserid` FOREIGN KEY (`IPUserID`) REFERENCES `users` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `individualprices` */

/*Table structure for table `logs` */

DROP TABLE IF EXISTS `logs`;

CREATE TABLE `logs` (
  `LogID` int(244) NOT NULL AUTO_INCREMENT,
  `LogUserID` int(244) NOT NULL,
  `LogDate` int(244) NOT NULL,
  `LogIPAddress` blob NOT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `merchant` */

DROP TABLE IF EXISTS `merchant`;

CREATE TABLE `merchant` (
  `MerchantPaypalEmail` blob NOT NULL,
  `MerchantSkrillEmail` blob NOT NULL,
  `MerchantSkrillSecret` blob NOT NULL,
  `MerchantWebsiteName` blob NOT NULL,
  `MerchantRecoveryEmail` blob NOT NULL,
  `MerchantNotificationEmail` blob NOT NULL,
  `MerchantCurrencySymbol` blob NOT NULL,
  `MerchantCurrencyName` blob NOT NULL,
  `MerchantMinDeposit` blob NOT NULL,
  `MerchantRequireSkype` enum('Yes','No') NOT NULL DEFAULT 'Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `merchant` */

insert  into `merchant`(`MerchantPaypalEmail`,`MerchantSkrillEmail`,`MerchantSkrillSecret`,`MerchantWebsiteName`,`MerchantRecoveryEmail`,`MerchantNotificationEmail`,`MerchantCurrencySymbol`,`MerchantCurrencyName`,`MerchantMinDeposit`,`MerchantRequireSkype`) values
('','','','Enigma - SMM Panel','recovery@email.com','notifications@email.com','$','USD','5','Yes');

/*Table structure for table `navigation` */

DROP TABLE IF EXISTS `navigation`;

CREATE TABLE `navigation` (
  `NavigationID` int(244) NOT NULL AUTO_INCREMENT,
  `NavigationText` blob NOT NULL,
  `NavigationURL` blob NOT NULL,
  `NavigationIcon` blob NOT NULL,
  PRIMARY KEY (`NavigationID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `navigation` */

/*Table structure for table `news` */

DROP TABLE IF EXISTS `news`;

CREATE TABLE `news` (
  `NewsID` int(244) NOT NULL AUTO_INCREMENT,
  `NewsTitle` blob NOT NULL,
  `NewsContent` blob NOT NULL,
  `NewsDate` int(244) NOT NULL,
  `NewsUserID` int(244) NOT NULL,
  PRIMARY KEY (`NewsID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `news` */

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `OrderID` int(244) NOT NULL AUTO_INCREMENT,
  `OrderUserID` int(244) NOT NULL,
  `OrderProductID` int(244) NOT NULL,
  `OrderDate` int(244) NOT NULL,
  `OrderAmount` blob NOT NULL,
  `OrderLink` blob NOT NULL,
  `OrderQuantity` int(244) NOT NULL,
  `OrderStatus` enum('Unprocessed','In Process','On hold','Refunded','Completed','Removed') NOT NULL DEFAULT 'Unprocessed',
  `OrderStartCount` int(244) DEFAULT NULL,
  `OrderType` enum('default','hashtag','comments','mentions') NOT NULL DEFAULT 'default',
  `OrderAdditional` blob,
  `OrderAPIID` int(244) DEFAULT NULL,
  PRIMARY KEY (`OrderID`),
  KEY `orderuserid` (`OrderUserID`),
  KEY `orderproductid` (`OrderProductID`),
  CONSTRAINT `orderproductid` FOREIGN KEY (`OrderProductID`) REFERENCES `products` (`ProductID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `orderuserid` FOREIGN KEY (`OrderUserID`) REFERENCES `users` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `ProductID` int(244) NOT NULL AUTO_INCREMENT,
  `ProductCategoryID` int(244) NOT NULL,
  `ProductName` blob NOT NULL,
  `ProductDescription` blob NOT NULL,
  `ProductMinimumQuantity` int(244) NOT NULL,
  `ProductMaxQuantity` int(244) NOT NULL,
  `ProductPrice` blob NOT NULL,
  `ProductAPI` blob NOT NULL,
  `ProductCreatedDate` int(244) NOT NULL,
  `ProductResellerPrice` blob NOT NULL,
  `ProductType` enum('default','hashtag','comments','mentions') NOT NULL DEFAULT 'default',
  PRIMARY KEY (`ProductID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `support` */

DROP TABLE IF EXISTS `support`;

CREATE TABLE `support` (
  `SupportID` int(244) NOT NULL AUTO_INCREMENT,
  `SupportUserID` int(244) NOT NULL,
  `SupportTitle` blob NOT NULL,
  `SupportMessage` blob NOT NULL,
  `SupportDate` int(244) NOT NULL,
  `SupportReply` blob NOT NULL,
  PRIMARY KEY (`SupportID`),
  KEY `supportuserid` (`SupportUserID`),
  CONSTRAINT `supportuserid` FOREIGN KEY (`SupportUserID`) REFERENCES `users` (`UserID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `UserID` int(244) NOT NULL AUTO_INCREMENT,
  `UserName` blob NOT NULL,
  `UserEmail` blob NOT NULL,
  `UserPassword` blob NOT NULL,
  `UserLevel` enum('default','banned','admin','reseller') CHARACTER SET latin1 NOT NULL DEFAULT 'default',
  `UserFirstName` blob NOT NULL,
  `UserLastName` blob NOT NULL,
  `UserRegistrationDate` int(244) NOT NULL,
  `UserRegistrationAddress` blob NOT NULL,
  `UserFunds` varchar(244) NOT NULL DEFAULT '0',
  `UserImage` blob,
  `UserAPI` blob,
  `UserSkype` blob NOT NULL,
  PRIMARY KEY (`UserID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `users` */

insert  into `users`(`UserID`,`UserName`,`UserEmail`,`UserPassword`,`UserLevel`,`UserFirstName`,`UserLastName`,`UserRegistrationDate`,`UserRegistrationAddress`,`UserFunds`,`UserImage`,`UserAPI`,`UserSkype`) values
(1,'admin','admin@dev.net','21232f297a57a5a743894a0e4a801fc3','admin','Admin','Admin',1478903870,'127.0.0.1','0',NULL,'78c2129ed4c5988451e3aac52c3aa6b9','admin');/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
