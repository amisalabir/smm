<?php
	require_once('./files/header.php');
	
	$UserAPI = $user->GetData('UserAPI');
?>
<link rel="stylesheet" type="text/css" href="js/bootstrap-fileupload/bootstrap-fileupload.css" />

<section id="main-content">
	<section class="wrapper">
		<div class="row">
			<div class="col-md-12">
				<section class="panel">
					<div class="panel-body profile-information">
						<div class="col-md-3">
							<div class="profile-pic text-center">
								<?php
									$user->GetImage();
								?>
							</div>
						</div>
						<div class="col-md-6">
							<div class="profile-desk">
								<h1>
									<?php
										echo(ucfirst($user->GetData('UserFirstName').' '));
										echo(ucfirst($user->GetData('UserLastName')));
										
										$UserID = $user->GetData('UserID');
										
										$stmt = $pdo->prepare('SELECT * FROM Logs WHERE LogUserID = :LogUserID ORDER BY LogID DESC LIMIT 1');
										$stmt->bindParam(':LogUserID', $UserID);
										$stmt->execute();
										
										$row = $stmt->fetch(PDO::FETCH_ASSOC);
										$login_date = $row['LogDate'];
										$login_ip_address = $row['LogIPAddress'];
									?>
								</h1>
								<span class="text-muted">
									<p>Welcome back, <?php echo(ucfirst($user->GetData('UserName'))); ?>.</p>
									<br>
									<p>You was logged at <b><?php echo(date('d M, Y H:i:s', $login_date)) ?></b> (<b><?php echo($login_ip_address); ?></b>).</p>
									
								</span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="profile-statistics">
								<h1>
									<?php
										$UserID = $user->GetData('UserID');
										$stmt = $pdo->prepare('SELECT * FROM orders WHERE OrderUserID = :OrderUserID');
										$stmt->bindParam(':OrderUserID', $UserID);
										$stmt->execute();
										
										echo $stmt->rowCount();
									?>
								</h1>
								<p>Account product orders.</p>
								<h1>$
									<?php
										$UserFunds = $user->GetData('UserFunds');
										echo round($UserFunds, 2);
									?>
								</h1>
								<p>Available account funds.</p>
							</div>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-12">
				<section class="panel">
					<header class="panel-heading tab-bg-dark-navy-blue">
						<ul class="nav nav-tabs nav-justified ">
							<li class="active">
								<a data-toggle="tab" href="#order-history">Order History</a>
							</li>
							<li>
								<a data-toggle="tab" href="#settings">Settings</a>
							</li>
						</ul>
					</header>
					<div class="panel-body">
						<div class="tab-content tasi-tab">
							<div id="order-history" class="tab-pane active">
								<div class="row">
									<div class="col-md-12">
										<?php
											$UserID = $user->GetData('UserID');
											$stmt = $pdo->prepare('SELECT * FROM orders WHERE OrderUserID = :OrderUserID ORDER BY OrderID DESC');
											$stmt->bindParam(':OrderUserID', $UserID);
											$stmt->execute();
											
											if($stmt->rowCount() > 0) {
												foreach($stmt->fetchAll() as $row) {
													$html = '<div class="msg-time-chat">';
													$html .= '<div class="message-body msg-in">';
													$html .= '<span class="arrow"></span>';
													$html .= '<div class="text">';
													$html .= '<div class="first">';
													$html .= date('d M, Y', $row['OrderDate']);
													$html .= '</div>';
													
													if($row['OrderStatus'] == 'Unprocessed') {
														$html .= '<div class="second bg-blue">';
													} else if($row['OrderStatus'] == 'Completed') {
														$html .= '<div class="second bg-green">';
													} else if($row['OrderStatus'] == 'In Process'){
														$html .= '<div class="second bg-yellow">';
													} else {
														$html .= '<div class="second bg-red">';
													}
													
													$html .= 'You\'ve purchased '.$product->GetData($row['OrderProductID'], 'ProductName').'.';
													$html .= '<p>Current status for this order is '.ucfirst($row['OrderStatus']).'.</p>';
													$html .= '</div>';
													$html .= '</div>';
													$html .= '</div>';
													$html .= '</div>';
													
													echo $html;
												}
											} else {
												$display->ReturnInfo('Your account does not have any orders at this time.');
											}
										?>
									</div>
								</div>
							</div>
							<div id="settings" class="tab-pane ">
								<div class="position-center">
									<div class="prf-contacts sttng">
										<h2>  Account API</h2>
									</div>
									<?php
										$UserAPI = $user->GetData('UserAPI');
									?>
									<form method="POST`" role="form" class="form-horizontal">
										<div class="form-group">
											<label class="col-lg-4 control-label">User API</label>
											<div class="col-lg-6">
												<input type="text" value="<?php echo($UserAPI); ?>" class="form-control" disabled>
											</div>
										</div>
									</form>
									<div class="prf-contacts sttng">
										<h2>  Account Avatar</h2>
									</div>
									<form method="POST" id="avatar-form" class="form-horizontal" enctype="multipart/form-data">
										<div class="form-group">
											<label class="col-lg-2 control-label"> Avatar</label>
											<div class="col-lg-6">
												<div class="fileupload fileupload-new" data-provides="fileupload">
													<span class="btn btn-white btn-file">
														<span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image to upload.</span>
														<span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
														<input type="file" id="avatar" name="avatar" accept="image/*" class="default">
													</span>
													<span class="fileupload-preview" style="margin-left:5px;"></span>
													<a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none; margin-left:5px;"></a>
												</div>
												<div style="margin-top: 5px;">
													<button type="submit" class="btn btn-primary" id="upload-avatar">Save</button>
													<div id="result"></div>
												</div>
											</div>
											
										</div>
										
									</form>
									<div class="prf-contacts sttng">
										<h2>  Account Information</h2>
									</div>
									<?php
										$UserName = $user->GetData('UserName');
										$UserEmail = $user->GetData('UserEmail');
										$UserFirstName = $user->GetData('UserFirstName');
										$UserLastName = $user->GetData('UserLastName');
										$UserSkypeID = $user->GetData('UserSkype');
									?>
									<form method="POST`" role="form" class="form-horizontal">
										<div class="form-group">
											<label class="col-lg-4 control-label">User Name</label>
											<div class="col-lg-6">
												<input type="text" value="<?php echo($UserName); ?>" class="form-control" disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Skype ID</label>
											<div class="col-lg-6">
												<input type="text" value="<?php echo($UserSkypeID); ?>" class="form-control" disabled>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">User E-mail</label>
											<div class="col-lg-6">
												<input type="text" id="email" value="<?php echo($UserEmail); ?>" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">User First Name</label>
											<div class="col-lg-6">
												<input type="text" id="first-name" placeholder="Your first name.." value="<?php echo($UserFirstName); ?>" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">User Last Name</label>
											<div class="col-lg-6">
												<input type="text" id="last-name" placeholder="Your last name.." value="<?php echo($UserLastName); ?>" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Account Password</label>
											<div class="col-lg-6">
												<input type="password" id="account-password" placeholder="Account password to confirm." value="" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-offset-2 col-lg-10">
												<button id="update-information" class="btn btn-primary" type="submit">Save</button>
												<input type="reset" class="btn btn-default" value="Cancel">
												<div id="account-update-result"></div>
											</div>
										</div>
									</form>
									
									<div class="prf-contacts sttng">
										<h2>  Account Password</h2>
									</div>
									<form method="POST" role="form" class="form-horizontal">
										<div class="form-group">
											<label class="col-lg-4 control-label">Account Current Password</label>
											<div class="col-lg-6">
												<input type="password" id="current-password" value="" class="form-control" autocomplete="off" required>
											</div>
										</div>
										<div class="form-group">
											<label class="col-lg-4 control-label">Account New Password</label>
											<div class="col-lg-6">
												<input type="password" id="new-password" value="" class="form-control" autcomplete="off" required>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-offset-2 col-lg-10">
												<button id="update-password" class="btn btn-primary" type="submit">Save</button>
												<input type="reset" class="btn btn-default" value="Cancel">
												<div id="account-password-result"></div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<button type="submit" id="page-refresh" class="btn btn-primary pull-right fa fa-refresh"></button>
					</div>
				</section>
			</div>
		</div>
	</section>
</section>
<?php
	require_once('./files/footer.php');
?>